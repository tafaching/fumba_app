import 'package:dio/dio.dart';

import 'headers_interceptor.dart';

class DioClient {
  static Dio getDioClient() {
    return Dio()
   // ..options = BaseOptions(baseUrl: 'https://fumbasuperstore.co.za/')
       ..options = BaseOptions(baseUrl: 'http://192.168.8.100:8888/mtc/')
     // ..options = BaseOptions(baseUrl: 'http://g40property.co.za/mall/mtc/')
      ..interceptors.add(HeadersInterceptor())
      ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
  }
}
