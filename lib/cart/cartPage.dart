import 'package:flutter/material.dart';
import 'package:fumba_app/cart/EmptyCartWidget.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';
import 'package:fumba_app/viewModel/cart_view_model.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String userIdx;
  String x;
  double cartTotal = 0.0;
  double cartTotals;

  // double cartTotals = 0.0;

  T sum<T extends num>(T lhs, T rhs) => lhs + rhs;

  @override
  void initState() {
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    setState(() {
      cartTotals = cartTotal;
    });
    Provider.of<CartViewModel>(context, listen: false).getCartByUserId();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.7;
    double widthFull = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        bottomNavigationBar: Material(
          elevation: 5.0,
          child: Container(
            color: Colors.white,
            width: widthFull,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ButtonTheme(
                  minWidth: ((widthFull) / 2),
                  height: 50.0,
                  child: RaisedButton(
                    child: Text(
                      'Pay Now',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ),
        body: wish(context));
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(
            "Alert",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          content: Text("No Item in Cart"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text(
                "Close",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget wish(BuildContext context) {
    Provider.of<CartViewModel>(context, listen: false).getCartByUserId();
    double width = MediaQuery.of(context).size.width * 0.7;
    return Consumer<CartViewModel>(
        builder: (context, wishListViewModel, child) {
      final x = wishListViewModel.cartList;
      if (x.isEmpty) {
        return EmptyCartWidget();
      } else {
        return ListView.builder(
            itemCount: wishListViewModel.cartList.length,
            itemBuilder: (context, index) {
              final item = wishListViewModel.cartList[index];

              List<double> a = [double.parse(item.price)];
              cartTotal = a.reduce(sum);
              // setState(() {
              //   cartTotals = cartTotal;
              // });
              return InkWell(
                onTap: () {},
                child: Dismissible(
                  key: Key('$item'),
                  onDismissed: (direction) {
                    setState(() {
                      wishListViewModel.deleteItemById(item.product_id);
                      wishListViewModel.cartList.removeAt(index);
                    });

                    // Then show a snackbar.
                    Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text("Item Removed from cart!")));
                  },
                  // Show a red background as the item is swiped away.
                  background: Container(color: Colors.red),

                  child: Container(
                    child: Card(
                        elevation: 3.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 120.0,
//                                height: ((height - 200.0) / 4.0),
                                  child: Image.network(
                                      PRODUCT_IMAGE_URL + item.photo),
                                )
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              width: (width - 20.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${item.name}',
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 7.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Price:',
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 15.0,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                        'R${item.price}',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15.0,
                                        ),
                                      ),

                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      InkWell(
                                        child: Container(
                                          color: Colors.grey,
                                          padding: EdgeInsets.all(3.0),
                                          child: Text(
                                            'Remove',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            wishListViewModel.deleteItemById(
                                                item.product_id.toString());
                                            wishListViewModel.cartList
                                                .removeAt(index);
                                          });
                                          // Then show a snackbar.
                                          Scaffold.of(context).showSnackBar(
                                              SnackBar(
                                                  content: Text(
                                                      "Item Removed from cart!")));
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                ),
              );
            });
      }
    });
  }
}
