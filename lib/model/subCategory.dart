import 'package:fumba_app/model/base_model.dart';

class SubCategory with ModelMapper {
  int id;
  String name;
  String slug;
  int status;
  String photo;
  int category_id;

//  "id": 18,
//  "category_id": 17,
//  "name": "Produce",
//  "slug": "produce",
//  "photo": "produce.png",
//  "status": 1

  SubCategory({
    this.id,
    this.name,
    this.slug,
    this.status,
    this.photo,
    this.category_id,
  });

  @override
  ModelMapper fromMap(map) {
    id = map["id"] ?? '';
    name = map["name"] ?? '';
    slug = map["slug"] ?? '';
    status = map["status"] ?? '';
    photo = map["photo"] ?? '';
    category_id = map["category_id"] ?? '';

    return this;
  }

  @override
  ModelMapper newInstance() {
    return SubCategory();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name': name,
      'slug': slug,
      'status': status,
      'photo': photo,
      'category_id': category_id,
    };
  }
}
