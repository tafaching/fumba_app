import 'base_model.dart';

class Order with ModelMapper {
  int id;
  int userId;
  String cart;
  String method;
  String shipping;
  String pickupLocation;
  String totalQty;
  String pay_amount;
  dynamic txnid;
  dynamic chargeId;
  String order_number;
  String paymentStatus;
  String customerEmail;
  String customerName;
  String customerCountry;
  String customerPhone;
  String customerAddress;
  String customerCity;
  String customerZip;
  dynamic shippingName;
  dynamic shippingCountry;
  dynamic shippingEmail;
  dynamic shippingPhone;
  dynamic shippingAddress;
  dynamic shippingCity;
  dynamic shippingZip;
  dynamic orderNote;
  dynamic couponCode;
  dynamic couponDiscount;
  String status;
  String createdAt;
  String updatedAt;
  dynamic affilateUser;
  dynamic affilateCharge;
  String currencySign;
  int currencyValue;
  int shippingCost;
  int packingCost;
  int tax;
  int dp;
  dynamic payId;
  int vendorShippingId;
  int vendorPackingId;

  Order(
      {this.id,
      this.userId,
      this.cart,
      this.method,
      this.shipping,
      this.pickupLocation,
      this.totalQty,
      this.pay_amount,
      this.txnid,
      this.chargeId,
      this.order_number,
      this.paymentStatus,
      this.customerEmail,
      this.customerName,
      this.customerCountry,
      this.customerPhone,
      this.customerAddress,
      this.customerCity,
      this.customerZip,
      this.shippingName,
      this.shippingCountry,
      this.shippingEmail,
      this.shippingPhone,
      this.shippingAddress,
      this.shippingCity,
      this.shippingZip,
      this.orderNote,
      this.couponCode,
      this.couponDiscount,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.affilateUser,
      this.affilateCharge,
      this.currencySign,
      this.currencyValue,
      this.shippingCost,
      this.packingCost,
      this.tax,
      this.dp,
      this.payId,
      this.vendorShippingId,
      this.vendorPackingId});

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    userId = map['userId'];
    cart = map['cart']?.toString() ?? '';
    method = map['method']?.toString() ?? '';
    shipping = map['shipping']?.toString() ?? '';
    pickupLocation = map['pickupLocation']?.toString() ?? '';
    totalQty = map['totalQty']?.toString() ?? '';
    pay_amount = map['pay_amount']?.toString() ?? '';
    txnid = map['txnid']?.toString() ?? '';
    chargeId = map['chargeId']?.toString() ?? '';
    order_number = map['order_number']?.toString() ?? '';
    paymentStatus = map['paymentStatus']?.toString() ?? '';
    customerEmail = map['customerEmail']?.toString() ?? '';
    customerName = map['customerName']?.toString() ?? '';
    customerCountry = map['customerCountry']?.toString() ?? '';
    customerPhone = map['customerPhone']?.toString() ?? '';
    customerAddress = map['customerAddress']?.toString() ?? '';
    customerCity = map['customerCity']?.toString() ?? '';
    customerZip = map['customerZip']?.toString() ?? '';
    shippingName = map['shippingName']?.toString() ?? '';
    shippingCountry = map['shippingCountry']?.toString() ?? '';
    shippingEmail = map['shippingEmail']?.toString() ?? '';
    shippingPhone = map['shippingPhone']?.toString() ?? '';
    shippingAddress = map['shippingAddress']?.toString() ?? '';
    shippingCity = map['shippingCity']?.toString() ?? '';
    shippingZip = map['shippingZip']?.toString() ?? '';
    orderNote = map['orderNote']?.toString() ?? '';
    couponCode = map['couponCode']?.toString() ?? '';
    couponDiscount = map['couponDiscount']?.toString() ?? '';
    status = map['status']?.toString() ?? '';
    createdAt = map['createdAt']?.toString() ?? '';
    updatedAt = map['updatedAt']?.toString() ?? '';
    affilateUser = map['affilateUser']?.toString() ?? '';
    affilateCharge = map['affilateCharge']?.toString() ?? '';
    currencySign = map['currencySign']?.toString() ?? '';
    currencyValue = map['currencyValue'];
    shippingCost = map['shippingCost'];
    packingCost = map['packingCost'];
    tax = map['tax'];
    dp = map['dp'];
    payId = map['payId']?.toString() ?? '';
    vendorShippingId = map['vendorShippingId'];
    vendorPackingId = map['vendorPackingId'];
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Order();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'userId': userId,
      'cart': cart,
      'method': method,
      'shipping': shipping,
      'pickupLocation': pickupLocation,
      'totalQty': totalQty,
      'pay_amount': pay_amount,
      'txnid': txnid,
      'chargeId': chargeId,
      'order_number': order_number,
      'paymentStatus': paymentStatus,
      'customerEmail': customerEmail,
      'customerName': customerName,
      'customerCountry': customerCountry,
      'customerPhone': customerPhone,
      'customerAddress': customerAddress,
      'customerCity': customerCity,
      'customerZip': customerZip,
      'shippingName': shippingName,
      'shippingCountry': shippingCountry,
      'shippingEmail': shippingEmail,
      'shippingPhone': shippingPhone,
      'shippingAddress': shippingAddress,
      'shippingCity': shippingCity,
      'shippingZip': shippingZip,
      'orderNote': orderNote,
      'couponCode': couponCode,
      'couponDiscount': couponDiscount,
      'status': status,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'affilateUser': affilateUser,
      'affilateCharge': affilateCharge,
      'currencySign': currencySign,
      'currencyValue': currencyValue,
      'shippingCost': shippingCost,
      'packingCost': packingCost,
      'tax': tax,
      'dp': dp,
      'payId': payId,
      'vendorShippingId': vendorShippingId,
      'vendorPackingId': vendorPackingId,
    };
  }
}
