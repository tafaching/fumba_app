import 'base_model.dart';

class User with ModelMapper {
  int id;
  String name;
  String address;
  String api_token;
  String affilate_code;
  String updated_at;
  String email;
  String phone;
  String password;
  String zip;
  String city;

  User(
      {this.id,
      this.name,
      this.updated_at,
      this.address,
      this.api_token,
      this.affilate_code,
      this.email,
      this.phone,this.zip,this.city,
      this.password});

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    name = map['name']?.toString() ?? '';
    updated_at = map['updated_at']?.toString() ?? '';
    email = map['email']?.toString() ?? '';
    password = map['password']?.toString() ?? '';
    phone = map['phone']?.toString() ?? '';
    address = map['address']?.toString() ?? '';
    zip = map['zip']?.toString() ?? '';
    city = map['city']?.toString() ?? '';
    api_token = map['api_token']?.toString() ?? '';
    affilate_code = map['affilate_code']?.toString() ?? '';

    return this;
  }

  @override
  ModelMapper newInstance() {
    return User();
  }

  Map toJson() {
    return {
      'id': id,
      'name': name,
      'updated_at': updated_at,
      'email': email,
      'phone': phone,
      'address': address,
      'zip': zip,
      'city': city,
      'api_token': api_token,
      'affilate_code': affilate_code,
      'password': password
    };
  }
}
