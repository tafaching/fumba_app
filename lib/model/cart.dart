import 'dart:ffi';

import 'package:fumba_app/model/base_model.dart';


class Cart with ModelMapper {
  int id;
  String name;
  String price;
  String user_id;
  String photo;
  String product_id;
  String created_at;
  String updated_at;

  Cart(
      {this.id,
      this.name,
      this.price,
      this.user_id,
      this.photo,
      this.product_id,
      this.created_at,
      this.updated_at});

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    name = map['name'];
    price = map['price'];
    user_id = map['user_id'];
    photo = map['photo'];
    product_id = map['product_id'];
    created_at = map['created_at'];
    updated_at = map['updated_at'];
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Cart();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'user_id': user_id,
      'photo': photo,
      'product_id': product_id,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }
}
