class Slider {
  String image;
  String button;
  String description;

  Slider({this.image, this.button, this.description});
}

class SliderList {
  List<Slider> _list;

  List<Slider> get list => _list;

  SliderList() {
    _list = [
      new Slider(
          image: 'assets/slider1.jpeg',
          button: 'Shop now',
          description: 'Fresh from the farm, at 50% OFF'),
      new Slider(
          image: 'assets/slider1.jpeg', button: 'Shop now', description: 'Succulent fruit, 30% every Friday'),
      new Slider(image: 'assets/slider2.jpeg', button: 'Shop now', description: 'Fumba bakery bring you fresh'),
    ];
  }
}
