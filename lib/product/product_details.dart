import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ravepay/flutter_ravepay.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fumba_app/config/app_properties.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/home_grid_layers/best_seller/best_seller_grid.dart';
import 'package:fumba_app/model/cart.dart';
import 'package:fumba_app/model/productModel.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/model/wish.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';
import 'package:fumba_app/user/login.dart';
import 'package:fumba_app/viewModel/cart_view_model.dart';
import 'package:fumba_app/viewModel/wish_view_model.dart';

class ProductDetails extends StatefulWidget {
  final ProductModel item;

  ProductDetails({Key key, this.item}) : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool favourite = false;
  Color color = Colors.grey;
  SharedPreferencesHelper sharedPreferencesHelper;
  CartViewModel cartViewModel;
  WishViewModel wishViewModel;
  User user;
  int userIdx;
  String x;
  int cartItem = 0;

  String namex;
  String photox;
  String emailx;
  String typex;
  String typeIdx;
  String passwordx;
  String phoneNumberx;

  @override
  void initState() {
    cartViewModel = CartViewModel();
    wishViewModel = WishViewModel();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    super.initState();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    setState(() {
      user = User().fromMap(jsonDecode(x));
      userIdx = user.id;
      emailx=user.email;
      namex=user.name;
      phoneNumberx=user.phone;
      return user;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget description = Padding(
      padding: const EdgeInsets.all(24.0),
      child: Text(
        widget.item.name,
        maxLines: 25,
        semanticsLabel: '...',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w300,
            fontStyle: FontStyle.normal,
            fontSize: 16.0),
      ),
    );

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(color: darkGrey),
          title: Text(
            widget.item.name ?? '',
            style: const TextStyle(
                color: darkGrey,
                fontWeight: FontWeight.w500,
                fontFamily: "Montserrat",
                fontSize: 18.0),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                details(context),

                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 150,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4.0),
                          border: Border.all(color: Colors.black, width: 0.5),
                        ),
                        child: Center(
                          child: new Text("Product Details",
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 16.0)),
                        ),
                      ),
                      RawMaterialButton(
                        onPressed: () {
                          if (x == null) {
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (c) => Login(),
                            ));
                          } else {
                            setState(() {
                              if (!favourite) {
                                favourite = true;
                                color = Colors.red;
                                wishViewModel.addToWish(
                                  Wish(
                                      product_id: widget.item.id.toString(),
                                      photo: widget.item.photo,
                                      name: widget.item.name,
                                      user_id: userIdx.toString(),
                                      price: widget.item.price.toString()),
                                );
                                _addWish(context);
                              } else {
                                favourite = false;
                                color = Colors.grey;
                                _removeWish(context);
                              }
                            });
                          }
                        },
                        constraints:
                            const BoxConstraints(minWidth: 45, minHeight: 45),
                        child: Icon(
                          (!favourite)
                              ? FontAwesomeIcons.heart
                              : FontAwesomeIcons.solidHeart,
                          color: color,
                        ),
                        elevation: 0.0,
                        shape: CircleBorder(),
                        fillColor: Colors.white,
                      ),
                    ],
                  ),
                ),
                description,

                Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 30),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 150,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4.0),
                              border:
                                  Border.all(color: Colors.black, width: 0.5),
                            ),
                            child: Center(
                              child: new Text("Other Products",
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0)),
                            ),
                          ),
                        ])),
                BestSellerGrid()
//                MoreProducts()
              ],
            ),
          ),
        ));
  }

  _addWish(BuildContext context) {
    final snackBar = SnackBar(content: Text('Added to favourite'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _removeWish(BuildContext context) {
    final snackBar = SnackBar(content: Text('Remove from favourite'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _addCart(BuildContext context) {
    final snackBar = SnackBar(content: Text('Item Added to Cart'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _paymentError(BuildContext context) {
    final snackBar = SnackBar(
        content: Text('We  are having a technical problem please try later'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Widget details(BuildContext context) {
    return Container(
      height: 250,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 16.0,
            child: Container(
                //TODO Adjust image  height
                margin: EdgeInsets.all(6.0),
                height: 200.0,
                child: Image.network(PRODUCT_IMAGE_URL + widget.item.photo)),
          ),
          Positioned(
            right: 0.0,
            child: Container(
              height: 180,
              width: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    onTap: () async {
//                      Navigator.of(context).push(MaterialPageRoute(builder: (_)=>CheckOutPage()));
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'R${widget.item.price}',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      if (x == null) {
                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (c) => Login(),
                        ));
                      } else {
                        payMethod();
                        // Navigator.of(context).push(
                        //     MaterialPageRoute(builder: (_) => Delivery()));
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'Buy Now',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (x == null) {
                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (c) => Login(),
                        ));
                      } else {
                        cartViewModel.addProperty(
                          Cart(
                              product_id: widget.item.id.toString(),
                              photo: widget.item.photo,
                              name: widget.item.name,
                              user_id: userIdx.toString(),
                              price: widget.item.price),
                        );
                        _addCart(context);
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'Add to cart',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  payMethod() async {
    Ravepay ravePay = Ravepay.of(_scaffoldKey.currentContext);
    RavepayResult result = await ravePay.chargeCard(
      RavepayConfig(
        amount: double.parse(widget.item.price),
        country: "ZA",
        currency: 'ZAR',
        email: emailx,
        firstname: namex,
        lastname: namex,
        phonenumber: phoneNumberx,
        narration: "Subscription Payment for Live Connected",
        publicKey:'FLWPUBK-21701f2b94bd82ba187e136ec03ca4dd-X',
        encryptionKey:'1d8ee3152ceb80d9879e845d',
        txRef: "testConnected-1234345",
        useAccounts: true,
        useCards: true,
        isStaging: false,
        useSave: true,
        metadata: [
          RavepayMeta("email", emailx),
        ],
      ),
    );

    if (result?.status == "SUCCESS") {
      print("Payment successful");
    } else {
      // _paymentError(context);
      print("Payment failed");
    }
  }
}
