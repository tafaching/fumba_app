import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fumba_app/cart/cartPage.dart';
import 'package:fumba_app/home_grid_layers/category/all_categories.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';
import 'package:fumba_app/user/account_page.dart';
import 'package:fumba_app/user/login.dart';
import 'package:fumba_app/wish_list/wishlist.dart';

import '../MainDashboard.dart';

// ignore: must_be_immutable
class TabsWidget extends StatefulWidget {
  int currentTab = 2;
  int selectedTab = 2;
  String currentTitle = 'Fumba Store';
  Widget currentPage = MainDashboard();

  TabsWidget({
    Key key,
    this.currentTab,
  }) : super(key: key);

  @override
  _TabsWidgetState createState() {
    return _TabsWidgetState();
  }
}

class _TabsWidgetState extends State<TabsWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  User user;
  SharedPreferencesHelper sharedPreferencesHelper;
  String userIdx;
  String namex;
  String photox;
  String emailx;
  String typex;
  String typeIdx;
  String passwordx;
  String phoneNumberx;
  String x;

  @override
  initState() {
    _selectTab(widget.currentTab);
    user = User();
    sharedPreferencesHelper = SharedPreferencesHelper();
    getUserId();
    super.initState();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    setState(() {
      user = User().fromMap(jsonDecode(x));

      setState(() {
        userIdx = user.id.toString();
        namex = user.name;
        emailx = user.email;

        return user;
      });
    });
  }

  @override
  void didUpdateWidget(TabsWidget oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      widget.selectedTab = tabItem;
      switch (tabItem) {
        case 0:
          if (x == null) {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Login(),
            ));
          } else {
            widget.currentTitle = 'Favourites';
            widget.currentPage = WishListPage();
          }

          break;
        case 1:
          widget.currentTitle = 'Categories';
          widget.currentPage = AllCategories();
          break;
        case 2:
          widget.currentTitle = 'Fumba Store';
          widget.currentPage = MainDashboard();
          break;
        case 3:
          if (x == null) {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Login(),
            ));
          } else {
            widget.currentTitle = 'Cart';
            widget.currentPage = CartPage();
          }

          break;
        case 4:


          if (x == null) {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Login(),
            ));
          } else {
            widget.currentTitle = 'Profile';
            widget.currentPage = AccountPage();
          }
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: Text(
          widget.currentTitle,
          style: Theme.of(context)
              .textTheme.headline
              .merge(TextStyle(fontWeight: FontWeight.w900)),
        ),
        centerTitle: true,
      ),
      body: widget.currentPage,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Theme.of(context).accentColor,
        selectedFontSize: 0,
        unselectedFontSize: 0,
        iconSize: 22,
        elevation: 0,
        backgroundColor: Colors.transparent,
        selectedIconTheme: IconThemeData(size: 25),
        unselectedItemColor: Theme.of(context).hintColor.withOpacity(1),
        currentIndex: widget.selectedTab,
        onTap: (int i) {
          this._selectTab(i);
        },
        // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border),
            title: new Container(height: 0.0),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            title: new Container(height: 0.0),
          ),
          BottomNavigationBarItem(
              title: new Container(height: 5.0),
              icon: Container(
                width: 45,
                height: 45,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.all(
                    Radius.circular(50),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context).accentColor.withOpacity(0.4),
                        blurRadius: 40,
                        offset: Offset(0, 15)),
                    BoxShadow(
                        color: Theme.of(context).accentColor.withOpacity(0.4),
                        blurRadius: 13,
                        offset: Offset(0, 3))
                  ],
                ),
                child:
                    new Icon(Icons.home, color: Theme.of(context).primaryColor),
              )),
          BottomNavigationBarItem(
            icon: new Icon(Icons.add_shopping_cart),
            title: new Container(height: 0.0),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.perm_identity),
            title: new Container(height: 0.0),
          ),
        ],
      ),
    );
  }
}
