
import 'package:flutter/material.dart';
import 'package:fumba_app/config/app_properties.dart';
import 'package:fumba_app/profile/settings/settings_page.dart';
import 'package:fumba_app/user/account_page.dart';

import 'faq_page.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Theme.of(context).primaryColor,
      body: SafeArea(
        top: true,
        child: SingleChildScrollView(
          child: Padding(
            padding:
                EdgeInsets.only(left: 16.0, right: 16.0, top: kToolbarHeight),
            child: Column(
              children: <Widget>[

                Container(
                  margin: EdgeInsets.symmetric(vertical: 16.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                          bottomLeft: Radius.circular(8),
                          bottomRight: Radius.circular(8)),
                      color: Colors.white,
                    border:
                    Border.all(color: Colors.grey, width: 0.5),

                      ),
                  height: 150,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              icon: new Icon(Icons.local_shipping,size: 40,),onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => AccountPage())),

                            ),
                            Text(
                              'Address',
                              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              icon: new Icon(Icons.person,size: 40,),onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => AccountPage())),

                            ),
                            Text(
                              'Contacts',
                              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),
                            )
                          ],
                        ),

                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              icon: new Icon(Icons.question_answer,size: 40,),onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => FaqPage())),
                            ),
                            Text(
                              'FAQ',
                              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
//                ListTile(
//                  title: Text('Settings'),
//                  subtitle: Text('Privacy and logout'),
//                  leading: Image.asset('assets/icons/settings_icon.png', fit: BoxFit.scaleDown, width: 30, height: 30,),
//                  trailing: Icon(Icons.chevron_right, color: darkGrey),
//                  onTap: () => Navigator.of(context).push(
//                      MaterialPageRoute(builder: (_) => SettingsPage())),
//                ),
//                Divider(),
//                ListTile(
//                  title: Text('Help & Support'),
//                  subtitle: Text('Help center and legal support'),
//                  leading: Image.asset('assets/icons/support.png'),
//                  trailing: Icon(
//                    Icons.chevron_right,
//                    color: darkGrey,
//                  ),
//                ),
//                Divider(),
//                ListTile(
//                  title: Text('FAQ'),
//                  subtitle: Text('Questions and Answer'),
//                  leading: Image.asset('assets/icons/faq.png'),
//                  trailing: Icon(Icons.chevron_right, color: darkGrey),
//                  onTap: () => Navigator.of(context).push(
//                      MaterialPageRoute(builder: (_) => FaqPage())),
//                ),
//                Divider(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
