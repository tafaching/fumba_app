import 'package:flutter/material.dart';
import 'package:fumba_app/config/custom_button.dart';
import 'package:fumba_app/constants/widget_factory.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/user/signup.dart';
import 'package:fumba_app/viewModel/login_view_model.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> implements LoginListener {
  GlobalKey<FormState> _userSettingsFormKey = new GlobalKey<FormState>();
  TextEditingController moduleEmailController = new TextEditingController();
  final TextEditingController modulePasswordController =
      new TextEditingController();
  LoginViewModel loginViewModel;

  @override
  void initState() {
    super.initState();
    loginViewModel = LoginViewModel();
  }

  @override
  Widget build(BuildContext context) {
    final logo = FlatButton(
      child: Text(
        'Please SignIn or SignUp for Free if you don\'t account with Fumba Store',
        style: TextStyle(
            fontSize: 30.0,
            color: Colors.grey,
            fontWeight: FontWeight.bold,
            fontFamily: 'Alatsi',
            height: 1.6),
        textAlign: TextAlign.center,
      ),
      onPressed: () {},
    );
    final registerLabel = Material(
        child: InkWell(
      onTap: () => {
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (c) => SignUp(),
        ))
      },
      child: Container(
        height: 60.0,
        width: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
        ),
        child: Center(
          child: Text(
            "SignUp",
            style: TextStyle(fontWeight: FontWeight.w600,fontSize: 20),
          ),
        ),
      ),
    ));
    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.white),
      ),
      onPressed: () {
//        Navigator.of(context).push(new MaterialPageRoute(
//          builder: (c) => ResetPassword(),
//        ));
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            Form(
                key: _userSettingsFormKey,
                child: Column(children: <Widget>[
                  createEmailField(
                      moduleEmailController,
                      "Email",
                      false,
                      ((input) =>
                          !input.contains('@') ? 'Not a valid email' : null),
                      (input) => moduleEmailController = input),
                  SizedBox(height: 8.0),
                  createInputValidationField(
                    modulePasswordController,
                    "Password",
                    false,
                    (input) => input.trim().length < 3
                        ? 'Password must have at least 4 characters'
                        : null,
                  ),
                ])),
            SizedBox(height: 24.0),

          Material(
              child: InkWell(
                onTap: () => {
                if (_userSettingsFormKey.currentState.validate()) {
                  loginViewModel.login(
                  User(
                  email: moduleEmailController.text,
                  password: modulePasswordController.text),
              this)
        }
                },
                child: Container(
                  height: 60.0,
                  width: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Center(
                    child: Text(
                      "SignIn",
                      style: TextStyle(fontWeight: FontWeight.w600,fontSize: 20),
                    ),
                  ),
                ),
              )),
            SizedBox(height: 24.0),
            registerLabel,
            forgotLabel
          ],
        ),
      ),
    );
  }

  @override
  void onFail() {
    Navigator.of(context).push(new MaterialPageRoute(
      builder: (c) => Login(),
    ));
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed('/Tabs', arguments: 2);
  }
}
