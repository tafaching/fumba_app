import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fumba_app/constants/widget_factory.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/viewModel/registration_view_model.dart';

import 'login.dart';

class SignUp extends StatefulWidget {
  @override
  SignUpState createState() {
    return new SignUpState();
  }
}

class SignUpState extends State<SignUp> implements RegistrationListener {
  GlobalKey<FormState> _userRegistrationFormKey = new GlobalKey<FormState>();

  final TextEditingController moduleNameController =
      new TextEditingController();

  TextEditingController moduleEmailController = new TextEditingController();

  final TextEditingController modulePhoneNumberController =
      new TextEditingController();
  final TextEditingController moduleAddressController =
  new TextEditingController();
  final TextEditingController modulePasswordController =
      new TextEditingController();

  RegistrationViewModel registrationViewModel;
  String phone;
  String format;

  @override
  void initState() {
    super.initState();
    registrationViewModel = RegistrationViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios,
              color: Theme.of(context).accentColor),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Login(),
            ));
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'SignUp',
          style: TextStyle(color: Theme.of(context).accentColor),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      body: new Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: new EdgeInsets.all(20.0),
        child: new Center(
            child: new Form(
          child: new ListView(
            children: <Widget>[
              Form(
                key: _userRegistrationFormKey,
                child: Column(
                  children: <Widget>[
                    createInputValidationField(
                      moduleNameController,
                      "Name",
                      false,
                      (input) => input.trim().length < 3
                          ? 'Not a valid full name'
                          : null,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    createNumberField(
                      modulePhoneNumberController,
                      "Phone number",
                      false,
                      (input) => input.trim().length < 10
                          ? 'Not a valid phone number'
                          : null,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    createEmailField(
                        moduleEmailController,
                        "Email",
                        false,
                        (moduleEmailController) =>
                            !moduleEmailController.contains('@')
                                ? 'Not a valid email'
                                : null,
                        (moduleEmailController) =>
                            moduleEmailController = moduleEmailController),
                    SizedBox(
                      height: 16,
                    ),
                    createInputValidationField(
                      moduleAddressController,
                      "Address",
                      false,
                          (input) => input.trim().length < 3
                          ? 'Not a valid Address'
                          : null,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    createInputValidationField(
                      modulePasswordController,
                      "Password",
                      false,
                      (input) => input.trim().length < 3
                          ? 'Password should not be less than 3 characters'
                          : null,
                    ),
                    SizedBox(
                      height: 26,
                    ),
                  ],
                ),
              ),
              Material(
                  child: InkWell(
                onTap: () => {
                  if (_userRegistrationFormKey.currentState.validate())
                    {
                      registrationViewModel.register(
                          User(
                            name: moduleNameController.text,
                            phone: modulePhoneNumberController.text,
                            password: modulePasswordController.text,
                            email: moduleEmailController.text,
                            address: moduleAddressController.text,
                          ),
                          this)
                    }
                },
                child: Container(
                  height: 60.0,
                  width: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Center(
                    child: Text(
                      "SignUp",
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                  ),
                ),
              )),
            ],
          ),
        )),
      ),
    );
  }

  @override
  void onFail() {
    _showDialog(context);
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed('/Tabs', arguments: 2);
  }
}

void _showDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(
              'User already exits, please reset your password if you forgot'),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('cancel'),
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Retry'),
            )
          ],
        );
      });
}
