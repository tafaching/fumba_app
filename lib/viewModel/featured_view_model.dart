import 'package:flutter/material.dart';
import 'package:fumba_app/model/productModel.dart';
import 'package:fumba_app/repository/generic_repository.dart';

class FeaturedViewModel extends ChangeNotifier with AFeaturedViewModel {
  String featured = 'api/featuredProduct/{id}';

  List<ProductModel> productList = [];

  void getFeatured() async {
    await GenericRepository<ProductModel>()
        .getAll(featured, ProductModel(), null, {'id': 1}).then((response) {
      productList = response;
    });
    notifyListeners();
  }
}

abstract class AFeaturedViewModel {
  void getFeatured();
}
