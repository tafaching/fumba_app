import 'package:flutter/material.dart';
import 'package:fumba_app/model/Slides.dart';
import 'package:fumba_app/model/category.dart';
import 'package:fumba_app/repository/generic_repository.dart';

class SlidesViewModel extends ChangeNotifier with ASlidesViewModel {
  String allSlides = 'api/allSlides';

  List<Slides> slideList = [];

  @override
  Future getAllSlides() async {

    await GenericRepository<Slides>()
        .getAll(allSlides, Slides(), null, null)
        .then((response) {
      slideList = response;
    });
    notifyListeners();
  }
}

abstract class ASlidesViewModel {
  void getAllSlides();
}
