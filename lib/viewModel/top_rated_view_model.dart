import 'package:flutter/material.dart';
import 'package:fumba_app/model/productModel.dart';
import 'package:fumba_app/repository/generic_repository.dart';

class TopRatedViewModel extends ChangeNotifier with ATopRatedViewModel {
  String topSeller = 'api/topSellerProduct/{id}';

  List<ProductModel> productList = [];


  void getTopRates() async {
    await GenericRepository<ProductModel>()
        .getAll(topSeller, ProductModel(), null, {'id': 1}).then((response) {
      productList = response;
    });
    notifyListeners();
  }
}

abstract class ATopRatedViewModel {
  void getTopRates();
}
