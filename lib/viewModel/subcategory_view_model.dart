import 'package:flutter/material.dart';
import 'package:fumba_app/model/subCategory.dart';
import 'package:fumba_app/repository/generic_repository.dart';

class SubCategoryViewModel extends ChangeNotifier with ASubCategoryViewModel {
  String categoryId = 'subCategoryById/{id}';
  String allSubCategories = 'api/subCategories';

  List<SubCategory> subCategoryList = [];

  @override
  Future allSubCategory() async {
    //get Group
    await GenericRepository<SubCategory>()
        .getAll(allSubCategories, SubCategory(), null, null)
        .then((response) {
      subCategoryList = response;
    });
    notifyListeners();
  }

  @override
  void getSubCategoryById(int id) async {
    //get properties
    await GenericRepository<SubCategory>()
        .getAll(categoryId, SubCategory(), null, {'id': id}).then((response) {
      subCategoryList = response;
    });
    notifyListeners();
  }
}

abstract class ASubCategoryViewModel {
  void allSubCategory();

  void getSubCategoryById(int id);
}
