import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fumba_app/model/cart.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/repository/generic_repository.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';

class CartViewModel extends ChangeNotifier with ACartViewModel {
  String addCart = 'api/addCart';
  String getCart = 'api/getCart/{user_id}';
  String deleteCart = 'api/cart/{user_id}';
  String deleteItem = 'api/item/{id}';
  String u;
  List<Cart> cartList = [];
  SharedPreferencesHelper sharedPreferencesHelper;

  @override
  void getCartByUserId() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    User user = User().fromMap(jsonDecode(x));
    u = user.id.toString();
    print("Cart  ID" + user.id.toString());
    print(user);

    //TODO REMOVE MAGIC STRING USER ID

    await GenericRepository<Cart>()
        .getAll(getCart, Cart(), null, {'user_id': u}).then((response) {
      cartList = response;
    });
    notifyListeners();
  }

  @override
  void deleteItemById(String id) async {
    //get properties
    await GenericRepository<Cart>()
        .deleteAll(deleteItem, Cart(), null, {'id': id}).then((response) {});
    notifyListeners();
  }

  @override
  void addProductCart(Cart cart) async {
    await GenericRepository<Cart>()
        .post(addCart, cart, null, null)
        .then((response) {})
        .catchError((onError) {
      notifyListeners();
    });
  }

  @override
  void addProperty(Cart properties) async {
    await GenericRepository<Cart>()
        .post(addCart, properties, null, null)
        .then((response) {
      String groupJson = jsonEncode(properties.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
    }).catchError((onError) {
      notifyListeners();
    });
  }
}

abstract class CartListener {
  void onSuccess();

  void onFail();
}

abstract class ACartViewModel {
  void getCartByUserId();

  void addProductCart(Cart cart);

  void addProperty(Cart properties);

  void deleteItemById(String id);
}
