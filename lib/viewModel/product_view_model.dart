import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fumba_app/model/category.dart';
import 'package:fumba_app/model/productModel.dart';
import 'package:fumba_app/repository/generic_repository.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';

class ProductViewModel extends ChangeNotifier with AProductViewModel {
  String categoryId = 'api/subCategoryById/{id}';

  String allProducts = 'all';

  List<ProductModel> productList = [];

  @override
  Future getAllProducts() async {
    await GenericRepository<ProductModel>()
        .getAll(allProducts, ProductModel(), null, null)
        .then((response) {
      productList = response;
    });
    notifyListeners();
  }

  @override
  void getProductByCategoryId(int id) async {
    await GenericRepository<ProductModel>()
        .getAll(categoryId, ProductModel(), null, {'id': id}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  // TODO -----ONCLICK CATEGORY LIST ON HOME PAGE
  @override
  void getProductByCaterId() async {
    final String x = await SharedPreferencesHelper().getCategory("CATEGORYID");
    print("CATEGORY  IYO" + x);
    Category category = Category().fromMap(jsonDecode(x));
    print("CATEGORY  ID" + category.id.toString());
    print(category);

    //get properties
    await GenericRepository<ProductModel>().getAll(
        categoryId, ProductModel(), null, {'id': category.id}).then((response) {
      productList = response;
    });
    notifyListeners();
  }
}

abstract class AProductViewModel {
  void getAllProducts();

  void getProductByCaterId();

  void getProductByCategoryId(int id);
}
