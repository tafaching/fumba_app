import 'package:flutter/material.dart';
import 'package:fumba_app/model/productModel.dart';
import 'package:fumba_app/repository/generic_repository.dart';

class BestSellerViewModel extends ChangeNotifier with ABestSellerViewModel {
  String bestSeller = 'api/bestSellerProduct/{id}';

  List<ProductModel> productList = [];

  void getBestSeller() async {
    await GenericRepository<ProductModel>()
        .getAll(bestSeller, ProductModel(), null, {'id': 1}).then((response) {
      productList = response;
    });
    notifyListeners();
  }
}

abstract class ABestSellerViewModel {
  void getBestSeller();
}
