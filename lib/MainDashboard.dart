import 'package:flutter/material.dart';

import 'home_grid_layers/best_seller/best_seller.dart';
import 'home_grid_layers/best_seller/best_seller_grid.dart';
import 'home_grid_layers/category/category_grid.dart';
import 'home_grid_layers/featured/featured.dart';
import 'home_grid_layers/featured/featured_grid.dart';
import 'home_grid_layers/top_rates/top_rates.dart';
import 'home_grid_layers/top_rates/top_rates_grid.dart';

class MainDashboard extends StatefulWidget {
  @override
  _MainDashboardState createState() => _MainDashboardState();
}

class _MainDashboardState extends State<MainDashboard> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        CategoryGrid(),
        TopRatingTitle(),

        SizedBox(
          height: 25.0,
        ),

        TopRatesGrid(),

        SizedBox(
          height: 4.2,
        ),

        SizedBox(
          height: 25.0,
        ),

        FeatureTitle(),

        SizedBox(
          height: 25.0,
        ),

        FeaturedGrid(),

        SizedBox(
          height: 25.0,
        ),

        BestSellerTitle(),

        SizedBox(
          height: 25.0,
        ),
        // Top Seller Grid Start Here
        BestSellerGrid(),
        // Top Seller Grid End Here

        SizedBox(
          height: 3.8,
        ),
      ],
    );
  }
}

class TopRatingTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          padding: EdgeInsets.all(0.0),
          width: 320.0,
          height: 30.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 0.4,
              ),
              Material(
                child: InkWell(
                  onTap: () => {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => TopRates(),
                    ))
                  },
                  child: Text(
                    'Top Ratings',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),
                  ),
                ),
              ),
              Material(
                child: InkWell(
                  onTap: () => {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => TopRates(),
                    ))
                  },
                  child: Container(
                    height: 60.0,
                    width: 100,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Center(
                      child: Text(
                        "See More",
                        style: TextStyle(fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class FeatureTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          padding: EdgeInsets.all(0.0),
          width: 320.0,
          height: 30.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 0.4,
              ),
              Material(
                child: InkWell(
                  onTap: () => {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => Featured(),
                    ))
                  },
                  child: Text(
                    'Featured',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),
                  ),
                ),
              ),
              Material(
                  child: InkWell(
                onTap: () => {
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (c) => Featured(),
                  ))
                },
                child: Container(
                  height: 60.0,
                  width: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Center(
                    child: Text(
                      "See More",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              )),
            ],
          ),
        ),
      ],
    );
  }
}

class BestSellerTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          padding: EdgeInsets.all(0.0),
          width: 320.0,
          height: 30.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 0.4,
              ),
              Material(
                child: InkWell(
                  onTap: () => {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => BestSeller(),
                    ))
                  },
                  child: Text(
                    'Best Seller',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),
                  ),
                ),
              ),
              Material(
                  child: InkWell(
                onTap: () => {
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (c) => BestSeller(),
                  ))
                },
                child: Container(
                  height: 60.0,
                  width: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Center(
                    child: Text(
                      "See More",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              )),
            ],
          ),
        ),
      ],
    );
  }
}
