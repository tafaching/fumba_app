import 'package:flutter/material.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/viewModel/wish_view_model.dart';
import 'package:fumba_app/wish_list/EmptyWishWidget.dart';
import 'package:provider/provider.dart';

class WishListPage extends StatefulWidget {
  @override
  _WishListPageState createState() => _WishListPageState();
}

class _WishListPageState extends State<WishListPage> {
  @override
  Widget build(BuildContext context) {
    Provider.of<WishViewModel>(context, listen: false).getWishByUserId();
    {
      return Scaffold(body: wish(context));
    }
  }

  Widget wish(BuildContext context) {
    Provider.of<WishViewModel>(context, listen: false).getWishByUserId();
    double width = MediaQuery.of(context).size.width * 0.7;
    return Consumer<WishViewModel>(
        builder: (context, wishListViewModel, child) {
      final x = wishListViewModel.wishList;
      if (x.isEmpty) {
        return EmptyWishWidget();
      } else {
        return ListView.builder(
            itemCount: wishListViewModel.wishList.length,
            itemBuilder: (context, index) {
              final item = wishListViewModel.wishList[index];
              return InkWell(
                onTap: () {
//                  Navigator.of(context).push(new MaterialPageRoute(
//                    builder: (c) => WishPropertyPage(item: item),
//                  ));
                },
                child: Dismissible(
                  key: Key('$item'),
                  onDismissed: (direction) {
                    setState(() {
                      wishListViewModel.deleteItemById(item.product_id);
                      wishListViewModel.wishList.removeAt(index);
                    });

                    // Then show a snackbar.
                    Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text("Item Removed from cart!")));
                  },
                  // Show a red background as the item is swiped away.
                  background: Container(color: Colors.red),

                  child: Container(
                    child: Card(
                        elevation: 3.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 120.0,
//                                height: ((height - 200.0) / 4.0),
                                  child: Image.network(
                                      PRODUCT_IMAGE_URL + item.photo),
                                )
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              width: (width - 20.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    '${item.name}',
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 7.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Price:',
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 15.0,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Text(
                                        'R${item.price}',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      InkWell(
                                        child: Container(
                                          color: Colors.grey,
                                          padding: EdgeInsets.all(3.0),
                                          child: Text(
                                            'Remove',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            wishListViewModel.deleteItemById(
                                                item.id.toString());
                                            wishListViewModel.wishList
                                                .removeAt(index);
                                          });
                                          // Then show a snackbar.
                                          Scaffold.of(context).showSnackBar(
                                              SnackBar(
                                                  content: Text(
                                                      "Item Removed from cart!")));
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                ),
              );
            });
      }
    });
  }
}
