import 'package:flutter/material.dart';
import 'package:fumba_app/splash/spashscreen.dart';
import 'package:fumba_app/tabs/tabs.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SplashScreen());


      case '/Tabs':
        return MaterialPageRoute(
            builder: (_) => TabsWidget(
              currentTab: args,
            ));

      default:
        return _errorRoute();
    }
  }


  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
