import 'package:flutter/material.dart';

import 'delivery_widget.dart';

class Delivery extends StatefulWidget {
  @override
  _DeliveryState createState() => _DeliveryState();
}

class _DeliveryState extends State<Delivery> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,

        title: Text(
          'Address Details',
          style: TextStyle(color: Theme.of(context).accentColor),
        ),
        centerTitle: true,
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: DeliveryWidget(),
    );
  }
}
