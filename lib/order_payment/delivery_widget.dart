import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fumba_app/Animation/slide_left_rout.dart';
import 'package:fumba_app/constants/widget_factory.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/order_payment/payment.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';
import 'package:fumba_app/viewModel/registration_view_model.dart';

class DeliveryWidget extends StatefulWidget {
  @override
  _DeliveryWidgetState createState() => _DeliveryWidgetState();
}

class _DeliveryWidgetState extends State<DeliveryWidget> implements RegistrationListener{
  User user;
  SharedPreferencesHelper sharedPreferencesHelper;
  int userIdx;
  String postalx;
  String cityx;
  String namex;
  String emailx;

  String addressx;
  String phoneNumberx;
  RegistrationViewModel registrationViewModel;
  String x;
  GlobalKey<FormState> _propertyFormKey = new GlobalKey<FormState>();
  TextEditingController postalCodeTextController = TextEditingController();
  TextEditingController cityTextController = TextEditingController();
  TextEditingController addressTextController = TextEditingController();
  TextEditingController phoneTextController = TextEditingController();
  GlobalKey<FormState> _userAddressSettingsFormKey = new GlobalKey<FormState>();

  @override
  void initState() {
    sharedPreferencesHelper = SharedPreferencesHelper();
    registrationViewModel=RegistrationViewModel();
    user = User();
    getUserId();
    super.initState();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    setState(() {
      user = User().fromMap(jsonDecode(x));

      setState(() {
        userIdx = user.id;
        postalx = user.zip;
        addressx = user.address;
        cityx = user.city;
        emailx=user.email;
        namex=user.name;
        phoneNumberx = user.phone;
        return user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: <Widget>[
          Container(
            padding: new EdgeInsets.all(20.0),
            child: Form(
                key: _propertyFormKey,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Where are your Ordered Item Shipped?',
                            style: TextStyle(
                                fontSize: 30.0,
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Alatsi',
                                height: 1.6),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: createInputValidationField(
                            addressTextController =
                                TextEditingController(text: addressx),
                            'Address',
                            false,
                            (input) => input.trim().length < 3
                                ? 'Address should not be less than 3 characters'
                                : null,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: createNumberField(
                            postalCodeTextController =
                                TextEditingController(text: postalx),
                            'Postal Code',
                            false,
                            (input) => input.trim().length < 2
                                ? 'Field should not be empty'
                                : null,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: createInputValidationField(
                            cityTextController =
                                TextEditingController(text: cityx),
                            'City',
                            false,
                            (input) => input.trim().length < 2
                                ? 'Field should not be empty'
                                : null,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: createInputValidationField(
                            phoneTextController =
                                TextEditingController(text: phoneNumberx),
                            'Phone Number',
                            false,
                            (input) => input.trim().length < 2
                                ? 'Field should not be empty'
                                : null,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 36,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(30.0),
                            child: InkWell(
                              onTap: () {
                                if (_propertyFormKey.currentState.validate()) {

                                  registrationViewModel.register(
                                      User(
                                        id: userIdx,
                                        name: namex,
                                        email: emailx,
                                        zip: postalCodeTextController.text,
                                        phone: phoneTextController.text,
                                        city: cityTextController.text,
                                        address: addressTextController.text,
                                      ),
                                      this);

                                }
                              },
                              child: Container(
                                width: width - 40.0,
                                padding: EdgeInsets.all(15.0),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border.all(
                                      color: Colors.black, width: 0.5),
                                ),
                                child: Text(
                                  'Go to Payment',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),

                        )
                      ],
                    ),
                  ],
                )),
          )
        ]);
  }

  @override
  void onFail() {
    // TODO: implement onFail
  }

  @override
  void onSuccess() {
    Navigator.push(context,
        SlideLeftRoute(page: PaymentPage()));
  }
}
