import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

createInputFieldWithIcon(
    IconData icon, var controller, var hint, bool obscure) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: const EdgeInsets.only(top: 10.0, bottom: 20),
      child: new TextFormField(
          enabled: true,
          controller: controller,
          obscureText: obscure,
          keyboardType: TextInputType.number,
          // Use email input type for emails.
          decoration: new InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(
                color: Colors.grey,
              ),
              icon: new Icon(icon),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

createTextView(var textControl) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: const EdgeInsets.all(10.0),
      child: Material(child: textControl));
}

createLocationSearch(var controller, var hint, var validator) {
  return Container(
    padding: EdgeInsets.only(top: 10, bottom: 10, right: 15.0),
    child: Material(
      borderRadius: BorderRadius.circular(2.0),
      color: Colors.white,
      child: TextFormField(
          controller: controller,
          validator: validator,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              prefixIcon:
                  Icon(Icons.location_on, color: Colors.grey, size: 30.0),
              contentPadding: EdgeInsets.all(10),
              hintText: hint,
              hintStyle: TextStyle(
                color: Colors.grey,
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))),
    ),
  );
}

createInputValidationField(
    var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.text,
          enabled: true,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              fillColor: Colors.grey,
              focusColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

createDescriptionField(var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(color: Colors.black),
          maxLines: null,
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.newline,
          enabled: true,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              fillColor: Colors.grey,
              focusColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

createInputNoValidationField(var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          style: TextStyle(color: Colors.black),
          maxLines: null,
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.newline,
          enabled: true,

          // Use email input type for emails.
          decoration: new InputDecoration(
            labelText: label,
            fillColor: Colors.grey,
            focusColor: Colors.grey,
            labelStyle: TextStyle(color: Colors.grey),
            hintStyle: TextStyle(color: Colors.grey),
          )));
}

disabledInputField(var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.text,
          enabled: false,

          // Use email input type for emails.
          decoration: new InputDecoration(
            labelText: label,
            fillColor: Colors.grey,
            focusColor: Colors.grey,
            labelStyle: TextStyle(color: Colors.grey),
            hintStyle: TextStyle(color: Colors.grey),
          )));
}

createEmailField(
    var controller, var label, bool obscure, var validator, var onSaved) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          onSaved: onSaved,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.emailAddress,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              focusColor: Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

createNumberField(var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(
            color: Colors.black,
          ),
          keyboardType: TextInputType.number,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              focusColor: Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

Widget _buildStatItem(String label, String count) {
  TextStyle _statLabelTextStyle = TextStyle(
    fontFamily: 'Roboto',
    color: Colors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.w200,
  );

  TextStyle _statCountTextStyle = TextStyle(
    color: Colors.black54,
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
  );

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text(
        count,
        style: _statCountTextStyle,
      ),
      Text(
        label,
        style: _statLabelTextStyle,
      ),
    ],
  );
}
