import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fumba_app/model/user.dart';
import 'package:fumba_app/sharedPref/shared_preferences_helper.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String x;

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    if (x != null) {
      Navigator.of(context).pushNamed('/Tabs', arguments: 2);
    } else {
//      Navigator.of(context).push(new MaterialPageRoute(
//        builder: (c) => Login(),
//      ));

      Navigator.of(context).pushNamed('/Tabs', arguments: 2);
    }
  }

  @override
  void initState() {
    super.initState();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    startTime();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    user = User().fromMap(jsonDecode(x));
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 100.0,
                child: Image.asset('assets/logo.jpeg'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
