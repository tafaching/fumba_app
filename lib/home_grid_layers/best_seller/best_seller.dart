import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:fumba_app/Animation/slide_left_rout.dart';
import 'package:fumba_app/cart/cartPage.dart';
import 'package:fumba_app/wish_list/wishlist.dart';


import 'get_best_seller.dart';

class BestSeller extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text("Best Seller"),centerTitle: true,elevation: 0.0,
        titleSpacing: 0.0,
      ),
      body: GetBestSeller(),
    );
  }
}
