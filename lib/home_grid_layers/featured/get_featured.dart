import 'package:flutter/material.dart';
import 'package:fumba_app/filters/filter_row.dart';

import 'featured_grid_view.dart';

class GetFeatured extends StatefulWidget {
  @override
  _GetFeaturedState createState() => _GetFeaturedState();
}

class _GetFeaturedState extends State<GetFeatured> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
//        FilterRow(),
//        Divider(
//          height: 1.0,
//        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: FeaturedGridView()),
      ],
    );
  }
}
