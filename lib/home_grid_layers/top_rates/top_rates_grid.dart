import 'package:flutter/material.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/product/product_details.dart';
import 'package:fumba_app/viewModel/top_rated_view_model.dart';
import 'package:provider/provider.dart';

class TopRatesGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor
      ),
      width: double.infinity,
      // height: 500.0,
      child: Column(
        children: <Widget>[
          BestOfferItems(),
        ],
      ),
    );
  }
}

class BestOfferItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    Provider.of<TopRatedViewModel>(context, listen: false).getTopRates();
    {
      return Column(
        children: <Widget>[
          SizedBox(
            width: width,
            height: 180.0,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 100.0,
                  decoration: BoxDecoration(color: Theme.of(context).primaryColor),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),

                  alignment: Alignment.center,
                  // Todo  this is card height
                  height: 180.0,
                  child: Consumer<TopRatedViewModel>(
                      builder: (context, productViewModel, child) {
                    return ListView(
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      padding: const EdgeInsets.all(0),
                      children: List.generate(
                          productViewModel.productList.length, (index) {
                        final item = productViewModel.productList[index];

                        return Material(
                            child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (c) => ProductDetails(
                                item: item,
                              ),
                            ));
                          },
                          child: Container(
                            width: 160,
                            height: 100,
                            margin: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 5.0,
                                  color: Colors.grey,
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(6.0),
                                    height: 100.0,
                                    child: Image.network(
                                        PRODUCT_IMAGE_URL + item.photo)),
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        '${item.name}',
                                        style: TextStyle(fontSize: 12.0),
                                        textAlign: TextAlign.center,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "R${item.price}",
                                            style: TextStyle(
                                                fontSize: 14.0,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(
                                            width: 7.0,
                                          ),
                                          Text(
                                            "R${item.previous_price}",
                                            style: TextStyle(
                                                fontSize: 13.0,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                color: Colors.grey),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ));
                      }),
                    );
                  }),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
