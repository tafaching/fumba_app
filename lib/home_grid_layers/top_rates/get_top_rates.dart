import 'package:flutter/material.dart';

import 'top_rates_grid_view.dart';

class GetTopRates extends StatefulWidget {
  @override
  _GetTopRatesState createState() => _GetTopRatesState();
}

class _GetTopRatesState extends State<GetTopRates> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
//        FilterRow(),
//        Divider(
//          height: 1.0,
//        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: TopRatesGridView()),
      ],
    );
  }
}
