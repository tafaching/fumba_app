import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:fumba_app/Animation/slide_left_rout.dart';
import 'package:fumba_app/cart/cartPage.dart';
import 'package:fumba_app/wish_list/wishlist.dart';


import 'get_top_rates.dart';

class TopRates extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text("Top Rating"),centerTitle: true,
        titleSpacing: 0.0,
        elevation: 0.0,
      ),

      body: GetTopRates(),
    );
  }
}
