import 'package:flutter/material.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/viewModel/subcategory_view_model.dart';
import 'package:provider/provider.dart';

import 'category_view.dart';

class AllCategories extends StatefulWidget {
  @override
  _AllCategoriesState createState() => _AllCategoriesState();
}

class _AllCategoriesState extends State<AllCategories> {
  @override
  void initState() {
    Provider.of<SubCategoryViewModel>(context, listen: false).allSubCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: _body(context),
    );
  }

  @override
  Widget _body(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    {
      return Container(
        margin: EdgeInsets.all(0.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(1.0),
        ),
        child: Consumer<SubCategoryViewModel>(
            builder: (context, productViewModel, child) {
          return GridView.count(
            shrinkWrap: true,
            primary: false,
            crossAxisSpacing: 0,
            mainAxisSpacing: 0,
            crossAxisCount: 3,
            childAspectRatio: ((width) / (height - 150.0)),
            children:
                List.generate(
                    productViewModel.subCategoryList.length, (index) {
                  final item = productViewModel.subCategoryList[index];

                  return Material(
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (c) => CategoryView(
                              item: item,
                            ),
                          ));
                        },
                        child: Container(
                          color: Theme.of(context).primaryColor,
                          width: 100,
                          height: 100,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.all(6.0),
                                  height: 100.0,
                                  child: Image.network(
                                      CATEGORY_IMAGE_URL + item.photo)),
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      '${item.name}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .display1
                                          .merge(TextStyle(fontWeight: FontWeight.w200)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ));
                }),
          );
        }),
      );
    }
  }
}
