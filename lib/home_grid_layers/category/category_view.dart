import 'package:flutter/material.dart';
import 'package:fumba_app/home_grid_layers/category/get_category.dart';
import 'package:fumba_app/model/subCategory.dart';

class CategoryView extends StatelessWidget {
  final SubCategory item;

  CategoryView({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(item.name ?? 'Fumba Store'),
        centerTitle: true,
        titleSpacing: 0.0,
        elevation: 0.0,
      ),
      body: GetCategory(item: item),
    );
  }
}
