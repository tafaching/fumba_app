import 'package:flutter/material.dart';
import 'package:fumba_app/filters/filter_row.dart';
import 'package:fumba_app/home_grid_layers/category/category_grid_view.dart';
import 'package:fumba_app/model/category.dart';
import 'package:fumba_app/model/subCategory.dart';

class GetCategory extends StatefulWidget {
  final SubCategory item;

  GetCategory({Key key, this.item}) : super(key: key);
  @override
  _GetCategoryState createState() => _GetCategoryState();
}

class _GetCategoryState extends State<GetCategory> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
//        FilterRow(),
//        Divider(
//          height: 1.0,
//        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: CategoryGridView(item:widget.item)),
      ],
    );
  }
}
