import 'package:flutter/material.dart';
import 'package:fumba_app/constants/Constant.dart';
import 'package:fumba_app/home_grid_layers/category/category_view.dart';
import 'package:fumba_app/viewModel/subcategory_view_model.dart';
import 'package:provider/provider.dart';

class CategoryGrid extends StatefulWidget {
  @override
  _CategoryGridState createState() => _CategoryGridState();
}

class _CategoryGridState extends State<CategoryGrid> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      width: double.infinity,
      // height: 500.0,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 5.0,
          ),
          CategoryItems(),
        ],
      ),
    );
  }
}

class CategoryItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    Provider.of<SubCategoryViewModel>(context, listen: false).allSubCategory();
    {
      return Column(
        children: <Widget>[
          SizedBox(
            width: width,
            height: 180.0,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 100.0,
                  decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),

                  alignment: Alignment.center,
                  // Todo  this is card height
                  height: 180.0,
                  child: Consumer<SubCategoryViewModel>(
                      builder: (context, productViewModel, child) {
                    return ListView(
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      padding: const EdgeInsets.all(0),
                      children: List.generate(
                          productViewModel.subCategoryList.length, (index) {
                        final item = productViewModel.subCategoryList[index];

                        return Material(
                            child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (c) => CategoryView(
                                item: item,
                              ),
                            ));
                          },
                          child: Container(
                            color: Theme.of(context).primaryColor,
                            width: 100,
                            height: 100,
                            child: Column(
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(6.0),
                                    height: 100.0,
                                    child: Image.network(
                                        CATEGORY_IMAGE_URL + item.photo)),
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        '${item.name}',
                                        style: Theme.of(context)
                                            .textTheme.display1
                                            .merge(TextStyle(fontWeight: FontWeight.w200)),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ));
                      }),
                    );
                  }),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
